#coding=utf8
import numpy as np
from PIL import Image
import cv2
from keras.models import load_model
import json
import SocketServer
import threading

BUFF = 1024
HOST = '0.0.0.0'
PORT = 6000

alphabet = "0123456789абвгдежзийк"

model = load_model("gesture40_model1.h5")


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = b""
        while True:
            tmp = self.request.recv(1024)
            data += tmp
            if "\r\r\n" in tmp:
                break
        cur_thread = threading.current_thread()
        print "Current thread: %s" % (cur_thread.name)
        self.request.send(photo(data))
        self.finish()


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    allow_reuse_address = True


def chunks(lst, count):
    for i in range(0, len(lst), count):
        yield lst[i:i + count]


def get_from_webcam(frame):
    img = Image.fromarray(frame)

    lst = np.array(img.getdata()).transpose()
    r, g, b = lst

    r = list(r)
    r = list(chunks(r, 32))
    r = list(chunks(r, 32))
    g = list(g)
    g = list(chunks(g, 32))
    g = list(chunks(g, 32))
    b = list(b)
    b = list(chunks(b, 32))
    b = list(chunks(b, 32))
    lst = [b[0], g[0], r[0]]
    #  t.show()
    # print np.array(lst)
    return np.array(lst)


def photo(file):
    file = file[:-3]
    try:
        photobytes = bytes(file)
        img = cv2.imdecode(np.frombuffer(photobytes, dtype=np.uint8), cv2.IMREAD_UNCHANGED)
        X_test = get_from_webcam(img)
        X_test = X_test.astype('float32')
        X_test /= 255
        gesture = model.predict(np.array([X_test]), verbose=0)
        gesture = enumerate(gesture[0])
        gesture = sorted(gesture, key=lambda x: x[1], reverse=True)
        gesture = map(lambda key: key[0], gesture)
        result = {}
        result['gesture_ordered_classes'] = gesture
        print json.dumps(result)
        return json.dumps(result)
    except BaseException as e:
        print('Error: ', e)
        return json.dumps({'error': e.message})


def main():

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address
    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server.socket.settimeout(3)
    try:
        server_thread = threading.Thread(target=server.serve_forever)
        # Exit the server thread when the main thread terminates
        server_thread.daemon = True
        server_thread.start()
        print "Server loop running in thread:", server_thread.name
        server.serve_forever()
    except KeyboardInterrupt:
        server.shutdown()
        server.server_close()

if __name__ == "__main__":
    main()

#!/usr/bin/env python
"""
Multi-threaded TCP Server
multithreadedServer.py acts as a standard TCP server, with multi-threaded integration, spawning a new thread for each
client request it receives.
This is derived from an assignment for the Distributed Systems class at Bennington College
"""

from argparse import ArgumentParser
from threading import Lock, Thread
from socket import SO_REUSEADDR, SOCK_STREAM, socket, SOL_SOCKET, AF_INET
import cv2
from PIL import Image
import numpy as np
from keras.models import load_model
import json
import time

model = load_model("gesture40_model1.h5")

def chunks(lst, count):
    for i in range(0, len(lst), count):
        yield lst[i:i + count]


def get_from_webcam(frame):
    img = Image.fromarray(frame)

    lst = np.array(img.getdata()).transpose()
    r, g, b = lst

    r = list(r)
    r = list(chunks(r, 32))
    r = list(chunks(r, 32))
    g = list(g)
    g = list(chunks(g, 32))
    g = list(chunks(g, 32))
    b = list(b)
    b = list(chunks(b, 32))
    b = list(chunks(b, 32))
    lst = [b[0], g[0], r[0]]
    #  t.show()
    # print np.array(lst)
    return np.array(lst)


def photo(file):
    file = file[:-3]
    try:
        photobytes = bytes(file)
        img = cv2.imdecode(np.frombuffer(photobytes, dtype=np.uint8), cv2.IMREAD_UNCHANGED)
        X_test = get_from_webcam(img)
        X_test = X_test.astype('float32')
        X_test /= 255
        gesture = model.predict(np.array([X_test]), verbose=0)
        gesture = enumerate(gesture[0])
        gesture = sorted(gesture, key=lambda x: x[1], reverse=True)
        gesture = map(lambda key: key[0], gesture)
        result = {}
        result['gesture_ordered_classes'] = gesture
        print json.dumps(result)
        return json.dumps(result)
    except BaseException as e:
        print('Error: ', e)
        return json.dumps({'error': e.message})


#---------------------------------------#
########## USER INPUT HANDLING ##########
#---------------------------------------#

# Initialize instance of an argument parser
parser = ArgumentParser(description='Multi-threaded TCP Server')

# Add optional argument, with given default values if user gives no arg
parser.add_argument('-p', '--port', default=6000, type=int, help='Port over which to connect')

# Get the arguments
args = parser.parse_args()

# -------------------------------------------#
########## DEFINE GLOBAL VARIABLES ##########
#-------------------------------------------#

counter = 0
response_message = "Now serving, number: "
thread_lock = Lock()

# Create a server TCP socket and allow address re-use
s = socket(AF_INET, SOCK_STREAM)
s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
s.bind(('0.0.0.0', args.port))

# Create a list in which threads will be stored in order to be joined later
threads = []

#---------------------------------------------------------#
########## THREADED CLIENT HANDLER CONSTRUCTION ###########
#---------------------------------------------------------#


class ClientHandler(Thread):
    def __init__(self, address, port, socket, response_message, lock):
        Thread.__init__(self)
        self.address = address
        self.port = port
        self.socket = socket
        self.response_message = response_message
        self.lock = lock

    # Define the actions the thread will execute when called.
    def run(self):
        global counter
        data = b""
        while True:
            tmp = self.socket.recv(1024)
            data += tmp
            if "\r\r\n" in tmp:
                break
        self.socket.send(photo(data))
        # Lock the changing of the shared counter value to prevent erratic multithread changing behavior
        with self.lock:
            counter += 1
        self.socket.close()

#-----------------------------------------------#
########## MAIN-THREAD SERVER INSTANCE ##########
#-----------------------------------------------#

# Continuously listen for a client request and spawn a new thread to handle every request
print "Starting on port %s !" % args.port
while 1:

    try:
        # Listen for a request
        s.listen(1)
        # Accept the request
        sock, addr = s.accept()
        # Spawn a new thread for the given request
        time.sleep(0.1)
        newThread = ClientHandler(addr[0], addr[1], sock, response_message, thread_lock)
        newThread.start()
        threads.append(newThread)
    except KeyboardInterrupt:
        print "\nExiting Server\n"
        break

# When server ends gracefully (through user keyboard interrupt), wait until remaining threads finish
for item in threads:
    item.join()
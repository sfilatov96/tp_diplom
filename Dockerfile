FROM ubuntu:16.04

COPY . /tp-tcp-neural-network
WORKDIR tp-tcp-neural-network



RUN apt-get update && apt-get install -y python python-pip libopencv-dev python-opencv libopencv-dev python-opencv libopenblas-dev  libcurl4-gnutls-dev libexpat1-dev gettext   libz-dev libssl-dev libblas-dev liblapack-dev gfortran
RUN pip install -r requirements.txt

EXPOSE 6000

RUN mkdir /root/.keras
COPY ./keras.json /root/.keras/
CMD python socket_server.py
import multiprocessing
import socket
import cv2
from keras.models import load_model
import json
import numpy as np
from PIL import Image


model = load_model("gesture40_model1.h5")

def chunks(lst, count):
    for i in range(0, len(lst), count):
        yield lst[i:i + count]


def get_from_webcam(frame):
    img = Image.fromarray(frame)

    lst = np.array(img.getdata()).transpose()
    r, g, b = lst

    r = list(r)
    r = list(chunks(r, 32))
    r = list(chunks(r, 32))
    g = list(g)
    g = list(chunks(g, 32))
    g = list(chunks(g, 32))
    b = list(b)
    b = list(chunks(b, 32))
    b = list(chunks(b, 32))
    lst = [b[0], g[0], r[0]]
    #  t.show()
    # print np.array(lst)
    return np.array(lst)


def photo(file):
    file = file[:-3]
    try:
        photobytes = bytes(file)
        img = cv2.imdecode(np.frombuffer(photobytes, dtype=np.uint8), cv2.IMREAD_UNCHANGED)
        X_test = get_from_webcam(img)
        X_test = X_test.astype('float32')
        X_test /= 255
        gesture = model.predict(np.array([X_test]), verbose=0)
        gesture = enumerate(gesture[0])
        gesture = sorted(gesture, key=lambda x: x[1], reverse=True)
        gesture = map(lambda key: key[0], gesture)
        result = {}
        result['gesture_ordered_classes'] = gesture
        print json.dumps(result)
        return json.dumps(result)
    except BaseException as e:
        print('Error: ', e)
        return json.dumps({'error': e.message})



def handle(connection, address):
    import logging
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger("process-%r" % (address,))
    try:
        logger.debug("Connected %r at %r", connection, address)
        data = b""
        while True:
            tmp = connection.recv(1024)
            data += tmp
            if "\r\r\n" in tmp:
                break
        connection.send(photo(data))
        logger.debug("Sent data")
    except:
        logger.exception("Problem handling request")
    finally:
        logger.debug("Closing socket")
        connection.close()

class Server(object):
    def __init__(self, hostname, port):
        import logging
        self.logger = logging.getLogger("server")
        self.hostname = hostname
        self.port = port

    def start(self):
        self.logger.debug("listening")
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.hostname, self.port))
        self.socket.listen(1)

        while True:
            conn, address = self.socket.accept()
            self.logger.debug("Got connection")
            process = multiprocessing.Process(target=handle, args=(conn, address))
            process.daemon = True
            process.start()
            self.logger.debug("Started process %r", process)

if __name__ == "__main__":
    import logging
    logging.basicConfig(level=logging.DEBUG)
    server = Server("0.0.0.0", 9000)
    try:
        logging.info("Listening")
        server.start()
    except:
        logging.exception("Unexpected exception")
    finally:
        logging.info("Shutting down")
        for process in multiprocessing.active_children():
            logging.info("Shutting down process %r", process)
            process.terminate()
            process.join()
    logging.info("All done")
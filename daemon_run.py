from new_socket_service import main

import daemon

with daemon.DaemonContext():
    main()